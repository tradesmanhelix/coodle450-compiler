/**
 * Main Class File:		Oodle.java
 * File:				SemanticChecker.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

import java.util.*;

import cps450.Globals;
import cps450.oodle.analysis.AnalysisException;
import cps450.oodle.analysis.DepthFirstAdapter;
import cps450.oodle.node.*;

/**
 * Class for checking a lexed and parsed Oodle source file for semantic errors and decorating the AST
 * 
 * @author Alex Smith
 */
public class SemanticChecker extends DepthFirstAdapter {
	
	// SemanticChecker Class Variable Declarations
	// ************************************************
	
	// DATA STRUCTURES
	private HashMap<Node, Type> typeMap = new HashMap<Node, Type>(); // Map for keeping track of the type of nodes
	private SymbolTable symtab = Globals.symtab; // Symbol table instance
	private MethodTracker methTracker = Globals.methTracker; // Class for keeping track of the attributes of methods processed
	
	// COUNTERS
	private int localVar_offset = 0; // Integer for keeping track of the offset of local variables
	private int parameter_offset = 0; // Integer for keeping track of the offset of parameters variables
	private int num_classes = 0; // Integer to keep track of the number of classes
	
	// FLAGS
	private boolean areWeInAnArgDeclList = false; // Flag used to keep track of traversal of argument declaration lists
	
	// ************************************************
	
	/**
	 * SemanticChecker class constructor; begins semantic checking by reading in the Oodle standard library
	 */
	public SemanticChecker() {
		readOodStdLib();
	}
	
	/**
	 * Method for reporting semantic errors to the user
	 * 
	 * @param line - Line number of the error
	 * @param col - Column number of the error
	 * @param msg - Error message to display
	 */
	private void reportError(int line, int col, String msg) {
		System.out.println(Globals.filename + ":" + line + "," + col + ":" + msg);
		Globals.incSemanticErrors();
	}

	/**
	 * Method for retrieving the type of a node from a POptType node
	 * 
	 * @param node - node to get type from
	 * @return
	 */
	public Type typeFromNode(POptType node) {

		PType p = ((AOptType) node).getType();
		return typeFromNode(p); // Cast to a PType and call typeFromNode(PType p)
	}

	/**
	 * Method for retrieving the type of a node from a PType node
	 * 
	 * @param p - node to get type from
	 * @return
	 */
	public Type typeFromNode(PType p) {

		if (p instanceof AArrType) {
			return Type.ARRAY;
		} else if (p instanceof ABooleanType) {
			return Type.BOOLEAN;
		} else if (p instanceof AIdType) {
			AIdType aType = (AIdType) p.clone();
			return new Type(aType.getId().getText());
		} else if (p instanceof AIntType) {
			return Type.INT;
		} else if (p instanceof AStringType) {
			return Type.STRING;
		} else {
			return Type.ERROR;
		}
	}
	
	/**
	 * Add the readint and writeint functions from the Oodle standard library to the symbol table
	 */
	@SuppressWarnings("unchecked")
	public void readOodStdLib() {		
		
		// Create and add readint and its instance var
		String readIntString = "readint";
		MethodDecl readMethod = new MethodDecl(readIntString, Type.INT, null, 0);
		symtab.push(readIntString, readMethod);
		methTracker.addMethod(readMethod);
		
		String inReader = "in";
		VarDecl inVar = new VarDecl(new Type("inreader"), Globals.no_offset);
		symtab.push(inReader, inVar);
		
		// Create and add writeint and its instance var
		String writeIdString = "writeint";
		LinkedList writeParams = new LinkedList();
		writeParams.add(Type.INT);		
		MethodDecl writeMethod = new MethodDecl(writeIdString, null, writeParams, 1);					
		symtab.push(writeIdString, writeMethod);
		methTracker.addMethod(writeMethod);
		
		String outWriter = "out";
		VarDecl outVar = new VarDecl(new Type("outwriter"), Globals.no_offset);
		symtab.push(outWriter, outVar);
	}

	/**
	 * Overridden In methods
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void inAMethodDecl(AMethodDecl node) {

		// reset the localVar_offset counter
		localVar_offset = -4; // Reset to -4 to account for the return address which has already been pushed onto the stack
		
		LinkedList tailList, typeList = new LinkedList();
		String id = node.getStid().getText();
		Type t;
		MethodDecl theMethod;

		// Iterate over argument declarations and put types into a list in the
		// symbol table w/method name as key

		if (node.getArgumentDeclList() != null) {
			AArgumentDeclList declList = (AArgumentDeclList) node.getArgumentDeclList();

			if (declList.getArgDecl() != null) {
				AArgDecl theDecl = (AArgDecl) declList.getArgDecl();
				tailList = declList.getArgDeclTail();

				typeList.add(typeFromNode(theDecl.getType()));

				for (Iterator it = tailList.iterator(); it.hasNext();) {
					AArgDeclTail tail = (AArgDeclTail) it.next();
					PType theType = ((AArgDecl) tail.getArgDecl()).getType();
					typeList.add(typeFromNode(theType));
				}
			}
		}

		if (node.getOptType() == null) {
			t = new Type("null");
		} else {
			t = typeFromNode(node.getOptType());
		}

		theMethod = new MethodDecl(id, t, typeList, typeList.size());

		// Push the method on to the symbol table and add it to the global method tracker (used in code generation)
		symtab.push(id, theMethod);
		methTracker.addMethod(theMethod);
		
		// Increment the current scope (move from class scope to local scope)
		symtab.beginScope();
	}	

	@Override
	public void inAClassDef(AClassDef node) {
		
		++num_classes;
		
		if(num_classes > 1) {
			reportError(node.getStid().getLine(), node.getStid().getPos(), "Multiple classes are not supported");
		}
		
		// Increment the current scope (move from global scope to class scope)
		symtab.beginScope();
	}

	/**
	 * Overridden Case methods
	 */
	@Override
	public void caseAMethodDecl(AMethodDecl node) {
		
        // Reset the parameter offset counter
        parameter_offset = 8; // reset to 8 to account for the return address and the return value
		
	      inAMethodDecl(node);
	        if(node.getStid() != null)
	        {
	            node.getStid().apply(this);
	        }
	        if(node.getLPar() != null)
	        {
	            node.getLPar().apply(this);
	        }
	        if(node.getArgumentDeclList() != null)
	        {
	        	areWeInAnArgDeclList = true;
	            node.getArgumentDeclList().apply(this); // we need to know that we're in an arg decl list to compute variable offsets correctly
	            areWeInAnArgDeclList = false;
	        }
	        
	        if(node.getRPar() != null)
	        {
	            node.getRPar().apply(this);
	        }
	        if(node.getOptType() != null)
	        {
	            node.getOptType().apply(this);
	        }
	        if(node.getIs() != null)
	        {
	            node.getIs().apply(this);
	        }
	        if(node.getN1() != null)
	        {
	            node.getN1().apply(this);
	        }
	        {
	            List<PVarDecl> copy = new ArrayList<PVarDecl>(node.getVarDecl());
	            for(PVarDecl e : copy)
	            {
	                e.apply(this);
	            }
	        }
	        if(node.getBegin() != null)
	        {
	            node.getBegin().apply(this);
	        }
	        if(node.getN2() != null)
	        {
	            node.getN2().apply(this);
	        }
	        {
	            List<PStmt> copy = new ArrayList<PStmt>(node.getStmt());
	            for(PStmt e : copy)
	            {
	                e.apply(this);
	            }
	        }
	        if(node.getEnd() != null)
	        {
	            node.getEnd().apply(this);
	        }
	        if(node.getEndid() != null)
	        {
	            node.getEndid().apply(this);
	        }
	        if(node.getN3() != null)
	        {
	            node.getN3().apply(this);
	        }
	        outAMethodDecl(node);
	}

	/**
	 * Overridden Out methods
	 */
	@Override
	public void outAAddExpr4(AAddExpr4 node) {
		
		// Type check an addition expression
		Type t1 = typeMap.get(node.getE1());
		Type t2 = typeMap.get(node.getE2());

		assert t1 != null && t2 != null;

		Type result;

		if (t1 == Type.INT && t2 == Type.INT) {
			result = Type.INT;
		} else {
			reportError(node.getPlusOp().getLine(), node.getPlusOp().getPos(), "The type of the first operand (" + t1 + ") is not compatible with the type of the second operand (" + t2 + ").");
			result = Type.ERROR;
		}

		typeMap.put(node, result);
	}

	@Override
	public void outAAndExpr1(AAndExpr1 node) {
		
		// Type check an AND expression
		Type t1 = typeMap.get(node.getE1());
		Type t2 = typeMap.get(node.getE2());

		assert t1 != null && t2 != null;

		Type result;

		if (t1 == Type.BOOLEAN && t2 == Type.BOOLEAN) {
			result = Type.BOOLEAN;
		} else {
			reportError(node.getAnd().getLine(), node.getAnd().getPos(), "The type of the first operand (" + t1 + ") is not compatible with the type of the second operand (" + t2 + ").");
			result = Type.ERROR;
		}

		typeMap.put(node, result);
	}

	@Override
	public void outAArgDecl(AArgDecl node) {		
		
		// Process and type check a list of argument declarations
		String id = node.getId().getText();
		Type t = typeFromNode(node.getType());
		ArgDecl theArgDecl = new ArgDecl(t, parameter_offset);
		parameter_offset += 4; // Increment the offset for the next argument

		Symbol s = symtab.lookup(id);

		if (s != null) {
			if (s.getScope() == symtab.getScope()) {
				reportError(node.getId().getLine(), node.getId().getPos(), "Use of redeclared variable: " + id);
			} else {
				symtab.push(id, theArgDecl);
			}

		} else {
			symtab.push(id, theArgDecl);
		}
	}

	@Override
	public void outAArrInxExpr9(AArrInxExpr9 node) {
		
		// Arrays are an unsupported feature
		typeMap.put(node, Type.ERROR);
		reportError(node.getQuart().getLine(), node.getQuart().getPos(), "Arrays are not supported.");
	}

	@Override
	public void outAAssignStmt(AAssignStmt node) {
		
		// Type check an assignment statement
		String id = node.getId().getText();
		Symbol sym = symtab.lookup(id);

		if (sym == null) {
			reportError(node.getId().getLine(), node.getId().getPos(), "Undeclared variable: " + id);
			return;
		}

		Type lhsType = sym.getDecl().type;
		Type rhsType = typeMap.get(node.getExpr());

		if (lhsType != rhsType) {
			reportError(node.getId().getLine(), node.getId().getPos(), "The type of the left-hand side (" + lhsType + ") is not compatible with the type of the right-hand side (" + rhsType + ")");
		}		
		
		if(node.getOptArrInx().size() != 0) {
			reportError(node.getId().getLine(), node.getId().getPos(), "Array indexing on the LHS is not supported.");
		}
		
		Globals.symbolMap.put(node.getId(), symtab.lookup(node.getId().getText()));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void outACallExpr9(ACallExpr9 node) {
		
		// Type check a call expression against the types of the parameters that the method is expecting
		LinkedList argsList = new LinkedList(), typeList = new LinkedList();
		Type theExprType;
		String id = node.getId().toString();
		Symbol sym = symtab.lookup(id);
		MethodDecl methDcl;
		Type t1, t2;

		// If not null, put type into type map
		
		if (sym == null) {
			
			id = node.getId().toString().trim();
			sym = symtab.lookup(id);
			
			if(sym == null) {
				reportError(node.getId().getLine(), node.getId().getPos(), "Undefined function called: " + id);
				return;
			}
		}
		
		// BIG PICTURE: Compare to arg decl list and ensure that the correct
		// types are being passed to the method
				
		// Put the types of all items in the ExprList into a LinkedList
				
		if(node.getExprList() == null) { // The arg list for the method is empty, so the call list should be empty too
			methDcl = (MethodDecl) sym.getDecl();
			argsList = methDcl.getArgsList();
			
			if(argsList != null) {
				if(argsList.size() != 0) {
					reportError(node.getId().getLine(), node.getId().getPos(), "Incorrect number of arguments supplied to: " + id);
				}
			}
		} else {		
				
			argsList = ((AExprList)node.getExprList()).getExprListTail();
			theExprType = typeMap.get(((AExprList)node.getExprList()).getExpr());
			
			typeList.add(theExprType);
					
			for (Iterator it = argsList.iterator(); it.hasNext(); ) {
				AExprListTail exprTail = (AExprListTail) it.next();				
				typeList.add(typeMap.get(exprTail.getExpr()));
			}			 

			methDcl = (MethodDecl) sym.getDecl();
			argsList = methDcl.getArgsList();
			
			if(argsList == null) {
				reportError(node.getId().getLine(), node.getId().getPos(), "Incorrect number of arguments supplied to: " + id);
			} else {
			 
				Iterator i2 = typeList.iterator();
					 
				// Compare the types in the two lists
					 // Issue an error if any problems
				for(Iterator it = argsList.iterator(); it.hasNext(); ) {
					t1 = (Type) it.next();
					 
					if(!i2.hasNext()) {
						reportError(node.getId().getLine(), node.getId().getPos(), "Incorrect number of arguments supplied to: " + id);
						return;
					} else {
						t2 = (Type)i2.next();
					}
					 
					if(t1 != t2) {
						reportError(node.getId().getLine(), node.getId().getPos(), "Incorrect argument(s) supplied to: " + id);
						return;
					}
				}	
			}
		}		
		
		typeMap.put(node, sym.getType());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void outACallStmt(ACallStmt node) {
		
		// Type check a call expression against the types of the parameters that the method is expecting
		LinkedList argsList = new LinkedList(), typeList = new LinkedList();
		Type theExprType;
		String id = node.getId().toString();
		Symbol sym = symtab.lookup(id);
		MethodDecl methDcl;
		Type t1, t2;

		if (sym == null) {
			
			id = node.getId().toString().trim();
			sym = symtab.lookup(id);
			
			if(sym == null) {
				reportError(node.getId().getLine(), node.getId().getPos(), "Undefined function called: " + id);
				return;
			}
		}

		// BIG PICTURE: Compare to arg decl list and ensure that the correct
		// types are being passed to the method
				
		// Put the types of all items in the ExprList into a LinkedList
				
		if(node.getExprList() == null) { // The arg list for the method is empty, so the call list should be empty too
			methDcl = (MethodDecl) sym.getDecl();
			argsList = methDcl.getArgsList();
			
			if(argsList != null) {
				if(argsList.size() != 0) {
					reportError(node.getId().getLine(), node.getId().getPos(), "Incorrect number of arguments supplied to: " + id);
				}
			}
		} else {		
				
			argsList = ((AExprList)node.getExprList()).getExprListTail();
			theExprType = typeMap.get(((AExprList)node.getExprList()).getExpr());
			
			typeList.add(theExprType);
					
			for (Iterator it = argsList.iterator(); it.hasNext(); ) {
				AExprListTail exprTail = (AExprListTail) it.next();				
				typeList.add(typeMap.get(exprTail.getExpr()));
			}			 

			methDcl = (MethodDecl) sym.getDecl();
			argsList = methDcl.getArgsList();
			
			if(argsList == null) {
				reportError(node.getId().getLine(), node.getId().getPos(), "Incorrect number of arguments supplied to: " + id);
			} else {
			 
				Iterator i2 = typeList.iterator();
					 
				// Compare the types in the two lists
					 // Issue an error if any problems
				for(Iterator it = argsList.iterator(); it.hasNext(); ) {
					t1 = (Type) it.next();
					 
					if(!i2.hasNext()) {
						reportError(node.getId().getLine(), node.getId().getPos(), "Incorrect number of arguments supplied to: " + id);
						return;
					} else {
						t2 = (Type)i2.next();
					}
					 
					if(t1 != t2) {
						reportError(node.getId().getLine(), node.getId().getPos(), "Incorrect argument(s) supplied to " + id + "; need:" + t1 + "; received:" + t2);
						return;
					}
				}	
			}
		}
		
		typeMap.put(node, sym.getType());
	}

	@Override
	public void outACatExpr3(ACatExpr3 node) {
		
		// Strings are not supported; however, we do type check them :)
		Type t1 = typeMap.get(node.getE1());
		Type t2 = typeMap.get(node.getE2());

		assert t1 != null && t2 != null;

		Type result;

		if (t1 == Type.STRING && t2 == Type.STRING) {
			result = Type.STRING;
		} else {
			reportError(node.getCatOp().getLine(), node.getCatOp().getPos(), "The type of the first operand (" + t1 + ") is not compatible with the type of the second operand (" + t2 + ")");
			result = Type.ERROR;
		}

		typeMap.put(node, result);
		
		// Arrays are an unsupported feature
		reportError(node.getCatOp().getLine(), node.getCatOp().getPos(), "Strings are not supported.");
	}

	@Override
	public void outADivExpr5(ADivExpr5 node) {
		
		// Type check a division expression
		Type t1 = typeMap.get(node.getE1());
		Type t2 = typeMap.get(node.getE2());

		assert t1 != null && t2 != null;

		Type result;

		if (t1 == Type.INT && t2 == Type.INT) {
			result = Type.INT;
		} else {
			reportError(node.getDivOp().getLine(), node.getDivOp().getPos(), "The type of the first operand (" + t1 + ") is not compatible with the type of the second operand (" + t2 + ")");
			result = Type.ERROR;
		}

		typeMap.put(node, result);
	}

	@Override
	public void outAEqExpr2(AEqExpr2 node) {
		
		// Type check an equality expression
		Type result;
		Type e1Type = typeMap.get(node.getE1());
		Type e2Type = typeMap.get(node.getE2());

		if (e1Type == Type.INT && e2Type == Type.INT) {
			result = Type.BOOLEAN;
		} else if (e1Type == Type.STRING && e2Type == Type.STRING) {
			result = Type.BOOLEAN;
		} else if (e1Type == e2Type) {
			result = Type.ERROR;
			reportError(node.getEqOp().getLine(), node.getEqOp().getPos(),
					"Operator > not compatible with type: " + e1Type.toString());
		} else {
			result = Type.ERROR;
			reportError(node.getEqOp().getLine(), node.getEqOp().getPos(), "The type of the first operand (" + e1Type + ") is not compatible with the type of the second operand (" + e2Type + ").");
		}

		typeMap.put(node, result);
	}

	@Override
	public void outAFalseLit(AFalseLit node) {
		typeMap.put(node, Type.BOOLEAN);
	}

	@Override
	public void outAGtEqExpr2(AGtEqExpr2 node) {
		
		// Type check a greater than or equal to expression
		Type result;
		Type e1Type = typeMap.get(node.getE1());
		Type e2Type = typeMap.get(node.getE2());

		if (e1Type == Type.INT && e2Type == Type.INT) {
			result = Type.BOOLEAN;
		} else if (e1Type == Type.STRING && e2Type == Type.STRING) {
			result = Type.BOOLEAN;
		} else if (e1Type == e2Type) {
			result = Type.ERROR;
			reportError(node.getGteqOp().getLine(), node.getGteqOp().getPos(),
					"Operator > not compatible with type: " + e1Type.toString());
		} else {
			result = Type.ERROR;
			reportError(node.getGteqOp().getLine(), node.getGteqOp().getPos(), "The type of the first operand (" + e1Type + ") is not compatible with the type of the second operand (" + e2Type + ").");
		}

		typeMap.put(node, result);
	}

	@Override
	public void outAGtExpr2(AGtExpr2 node) {
		
		// Type check a greater than expression
		Type result;
		Type e1Type = typeMap.get(node.getE1());
		Type e2Type = typeMap.get(node.getE2());

		if (e1Type == Type.INT && e2Type == Type.INT) {
			result = Type.BOOLEAN;
		} else if (e1Type == Type.STRING && e2Type == Type.STRING) {
			result = Type.BOOLEAN;
		} else if (e1Type == e2Type) {
			result = Type.ERROR;
			reportError(node.getGtOp().getLine(), node.getGtOp().getPos(),
					"Operator > not compatible with type: " + e1Type.toString());
		} else {
			result = Type.ERROR;
			reportError(node.getGtOp().getLine(), node.getGtOp().getPos(), "The type of the first operand (" + e1Type + ") is not compatible with the type of the second operand (" + e2Type + ").");
		}

		typeMap.put(node, result);
	}

	@Override
	public void outAIdExpr9(AIdExpr9 node) {
		
		// Type check an identifier expression
		Type result = Type.ERROR;
		String id = node.getE().getText();
		Symbol s = symtab.lookup(id);
		
		if (s == null) {
			reportError(node.getE().getLine(), node.getE().getPos(), "Undefined variable: " + id);
		} else {
			result = s.getDecl().type;
		}

		typeMap.put(node, result);
		Globals.symbolMap.put(node, s);
	}

	@Override
	public void outAIfStmt(AIfStmt node) {
		Type whileCond = typeMap.get((node.getExpr()));

		if (whileCond != Type.BOOLEAN) {
			reportError(node.getI1().getLine(), node.getI1().getPos(), "If statements use boolean conditions; received:  " + whileCond);
		}
	}

	@Override
	public void outAInheritsFrom(AInheritsFrom node) {
		
		// Inheritance is an unsupported feature
		reportError(node.getId().getLine(), node.getId().getPos(), "The inherits from clause is not supported.");
	}

	@Override
	public void outAIntLit(AIntLit node) {
		typeMap.put(node, Type.INT);
	}

	@Override
	public void outALitExpr9(ALitExpr9 node) {
		typeMap.put(node, typeMap.get(node.getE()));
	}

	@Override
	public void outALoopStmt(ALoopStmt node) {
		Type whileCond = typeMap.get((node.getExpr()));

		if (whileCond != Type.BOOLEAN) {
			reportError(node.getWhile().getLine(), node.getWhile().getPos(), "Loop statements use boolean conditions; received:  " + whileCond);
		}
	}

	@Override
	public void outAMeExpr9(AMeExpr9 node) {
		
		// Objects are an unsupported feature
		typeMap.put(node, Type.ERROR);
		reportError(node.getMe().getLine(), node.getMe().getPos(), "The me keyword is not supported.");
	}

	@Override
	public void outAMethodDecl(AMethodDecl node) {
		// pops all items from the current scope off the stack and then
		// decrements the scope (error if scope < 0)
		try {
			symtab.endScope();
		} catch (AnalysisException e) {
			System.err.println(e.toString());
		}
	}

	@Override
	public void outAMultExpr5(AMultExpr5 node) {
		
		// Type check a multiplication expression
		Type t1 = typeMap.get(node.getE1());
		Type t2 = typeMap.get(node.getE2());

		assert t1 != null && t2 != null;

		Type result;

		if (t1 == Type.INT && t2 == Type.INT) {
			result = Type.INT;
		} else {
			reportError(node.getMultOp().getLine(), node.getMultOp().getPos(), "The type of the first operand (" + t1 + ") is not compatible with the type of the second operand (" + t2 + ")");
			result = Type.ERROR;
		}

		typeMap.put(node, result);
	}

	@Override
	public void outANegExpr6(ANegExpr6 node) {
		
		// Type check a negation expression
		Type t = typeMap.get(node.getE());
		
		if(t == null || t != Type.INT) {
			typeMap.put(node, Type.ERROR);
		} else {		
			typeMap.put(node, Type.INT);
		}
	}

	@Override
	public void outANewExpr9(ANewExpr9 node) {
		
		// Objects are an unsupported feature
		typeMap.put(node, Type.ERROR);
		reportError(node.getNewKw().getLine(), node.getNewKw().getPos(), "The new keyword is not supported.");
	}

	@Override
	public void outANotExpr6(ANotExpr6 node) {
		
		// Type check a not expression
		Type t = typeMap.get(node.getE());
		
		if(t == null || t != Type.BOOLEAN) {
			typeMap.put(node, Type.ERROR);
		} else {		
			typeMap.put(node, Type.BOOLEAN);
		}
	}

	@Override
	public void outANullLitLit(ANullLitLit node) {
		
		// Objects are an unsupported feature
		typeMap.put(node, Type.ERROR);
		reportError(node.getNullKw().getLine(), node.getNullKw().getPos(), "The null keyword is not supported.");
	}

	@Override
	public void outAOptArrInx(AOptArrInx node) {
		
		// Arrays are an unsupported feature
		typeMap.put(node, Type.ERROR);
		reportError(node.getLBrkt().getLine(), node.getLBrkt().getPos(), "Arrays are not supported.");
	}

	@Override
	public void outAOptExp(AOptExp node) {
		if(node.getExpr() != null) {
			reportError(node.getAssignOp().getLine(), node.getAssignOp().getPos(), "Variables with initializer expressions are not supported.");
		}
	}

	@Override
	public void outAOptType(AOptType node) {
		Type t = typeMap.get(node.getType());
		
		if(t == null) {
			reportError(node.getColon().getLine(), node.getColon().getPos(), "Class-level variables must be defined with a type.");
			typeMap.put(node, Type.ERROR);
		} else {
			typeMap.put(node, t);
		}		
	}

	@Override
	public void outAOrExpr(AOrExpr node) {
		
		// Type check an OR expression
		Type t1 = typeMap.get(node.getE1());
		Type t2 = typeMap.get(node.getE2());

		assert t1 != null && t2 != null;

		Type result;

		if (t1 == Type.BOOLEAN && t2 == Type.BOOLEAN) {
			result = Type.BOOLEAN;
		} else {
			reportError(node.getOr().getLine(), node.getOr().getPos(), "The type of the first operand (" + t1 + ") is not compatible with the type of the second operand (" + t2 + ").");
			result = Type.ERROR;
		}

		typeMap.put(node, result);
	}

	// *************************************************************
	// We simply put the types of these nodes into the type map so we can look their type up later
	@Override
	public void outAOtherExpr(AOtherExpr node) {
		typeMap.put(node, typeMap.get(node.getE()));
	}

	@Override
	public void outAOtherExpr1(AOtherExpr1 node) {
		typeMap.put(node, typeMap.get(node.getE()));
	}

	@Override
	public void outAOtherExpr2(AOtherExpr2 node) {
		typeMap.put(node, typeMap.get(node.getE()));
	}

	@Override
	public void outAOtherExpr3(AOtherExpr3 node) {
		typeMap.put(node, typeMap.get(node.getE()));
	}

	@Override
	public void outAOtherExpr4(AOtherExpr4 node) {
		typeMap.put(node, typeMap.get(node.getE()));
	}

	@Override
	public void outAOtherExpr5(AOtherExpr5 node) {
		typeMap.put(node, typeMap.get(node.getE()));
	}

	@Override
	public void outAOtherExpr6(AOtherExpr6 node) {
		typeMap.put(node, typeMap.get(node.getE()));
	}

	@Override
	public void outAParExpr9(AParExpr9 node) {
		typeMap.put(node, typeMap.get(node.getE()));
	}
	// *************************************************************

	@Override
	public void outAPosExpr6(APosExpr6 node) {
		
		// Type check a positivie expression
		Type t = typeMap.get(node.getE());
		
		if(t == null || t != Type.INT) {
			typeMap.put(node, Type.ERROR);
		} else {		
			typeMap.put(node, Type.INT);
		}
	}

	@Override
	public void outAStrLit(AStrLit node) {
		
		// Strings are an unsupported feature
		reportError(node.getE().getLine(), node.getE().getPos(),"Strings are not supported.");
		typeMap.put(node, Type.STRING);
	}

	@Override
	public void outASubExpr4(ASubExpr4 node) {
		
		// Type check a subtraction expression
		Type t1 = typeMap.get(node.getE1());
		Type t2 = typeMap.get(node.getE2());

		assert t1 != null && t2 != null;

		Type result;

		if (t1 == Type.INT && t2 == Type.INT) {
			result = Type.INT;
		} else {
			reportError(
					node.getMinusOp().getLine(),
					node.getMinusOp().getPos(),
					"The type of the first operand ("
							+ t1
							+ ") is not compatible with the type of the second operand ("
							+ t2 + ").");
			result = Type.ERROR;
		}

		typeMap.put(node, result);
	}

	@Override
	public void outATrueLit(ATrueLit node) {
		Type result = Type.BOOLEAN;
		typeMap.put(node, result);
	}

	@Override
	public void outAVarDecl(AVarDecl node) {
		String id = node.getId().getText();
		Type t = Type.ERROR;
		VarDecl theVarDecl;
						
		// Get the type of the variable being declared
		if(node.getOptType() != null) {
			t = typeMap.get(((AOptType) node.getOptType()).getType());
		}
		
		// Determine the variable's offset (if necessary)
		if(symtab.getScope() == Globals.local_scope) {
			if(areWeInAnArgDeclList) {
				localVar_offset -= 4;
				theVarDecl = new VarDecl(t, localVar_offset);
			} else {
				parameter_offset += 4;
				theVarDecl = new VarDecl(t, parameter_offset);
			}
		} else {
			theVarDecl = new VarDecl(t, Globals.no_offset);
		}

		Symbol s = symtab.lookup(id);

		// Check for variable declaration errors
		if (s != null) {						
			
			if (s.getScope() == symtab.getScope()) {
				reportError(node.getId().getLine(), node.getId().getPos(), "Use of redeclared variable: " + id);
				return; // Do not add the second declaration
			}		
			
			id = (id + "_" + symtab.getScope()).toString();
			TId convertID = new TId(id);
			node.setId(convertID);
			symtab.push(id, theVarDecl);
			Globals.symbolMap.put(node, s);
			
		} else {
			symtab.push(id, theVarDecl);
			Globals.symbolMap.put(node, s);
		}
	}

	/**
	 * Type Overrides
	 * 
	 * @param node - The name of the node we're visiting
	 */
	@Override
	public void outAArrType(AArrType node) {
		typeMap.put(node, Type.ERROR);
		reportError(node.getLBrkt().getLine(), node.getLBrkt().getPos(), "The array type is not supported.");
	}

	@Override
	public void outABooleanType(ABooleanType node) {
		typeMap.put(node, Type.BOOLEAN);
	}

	@Override
	public void outAIdType(AIdType node) {
		typeMap.put(node, new Type(node.getId().getText()));
	}

	@Override
	public void outAIntType(AIntType node) {
		typeMap.put(node, Type.INT);
	}

	@Override
	public void outAStringType(AStringType node) {
		typeMap.put(node, Type.STRING);
	}

}
