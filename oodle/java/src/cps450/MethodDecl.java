/**
 * Main Class File:		Oodle.java
 * File:				MethodDecl.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

import java.util.LinkedList;
import cps450.oodle.node.PType;

/**
 * Class for handling Oodle method declarations and their attributes
 * 
 * @author Alex Smith
 */
public class MethodDecl extends Declaration {

	private LinkedList<PType> argsList; // LinkedList of a method's arguments 
	private String name; // The name of the method
	private int numParams; // The number of parameters the method has (used primarily in code generation)

	/**
	 * MethodDecl constructor, which sets the:
	 * 
	 * @param name - of the method 
	 * @param returnType - of the method
	 * @param argsList - list of any arguments the method expects, and
	 * @param numParms - number of parameters the method takes
	 */
	public MethodDecl(String name, Type returnType, LinkedList<PType> argsList, int numParms) {		
		super(returnType);
		this.name = name;
		this.argsList = argsList;
		this.numParams = numParms;
	}

	/**
	 * Getter method for a method's name
	 * 
	 * @return name - The method's name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets the type of a method
	 * 
	 * @return type - The method's type
	 */
	public Type getType() {
		return this.type;
	}
	
	/**
	 * Gets a method's list of arguments
	 * 
	 * @return argsList - The list of arguments a method expects when it is called
	 */
	public LinkedList<PType> getArgsList() {
		return argsList;
	}
	
	/**
	 * Gets the number of parameters that a method is expecting
	 * 
	 * @return numParams
	 */
	public int getNumParams() {
		return numParams;
	}
		
}
