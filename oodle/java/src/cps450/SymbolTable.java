/**
 * Main Class File:		Oodle.java
 * File:				SymbolTable.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

import java.util.*;
import cps450.oodle.analysis.AnalysisException;
import cps450.Declaration;

/**
 * The SymbolTable class is used in semantic checking and allows the compiler to 
 * support multiple scopes.  It is also used to do error checking.
 * 
 * @author Alex Smith
 */
public class SymbolTable {
	
	// Class variables
	private Deque<Symbol> symbolStack = new LinkedList<Symbol>(); // Stack symbols are pushed on and off of as we change scopes and process the tree	
	private int currScope; // Tracks the current scope of the tokens being checked
	
	// Constructor
	public SymbolTable() {
		
		// Set the scope to zero (global scope) for starters
		currScope = Globals.global_scope;
	}	 

	// Used to push a symbol onto the symbol table "stack"
	public Symbol push(String name, Declaration decl) {
		Symbol sym = new Symbol(name, decl, currScope);
		symbolStack.push(sym);
		return sym;
	}
		
	// Finds a symbol in the symbol table
	public Symbol lookup(String  name) {
		Iterator<Symbol> itr = symbolStack.iterator();
		Symbol s;
		
		while(itr.hasNext()) {
			s = (Symbol)itr.next();
			if(s.getName().equals(name)) {
				return s;
			}
		}
		
		// If the symbol isn't in the symbol table, return null
		s = null; 
		return s;
	}
	
	// Begins a new scope by incrementing the current scope
	public void beginScope() {
		++currScope;
	}
	
	// Ends the current scope by popping all symbols in the current scope off the symbol stack and decrementing the scope
	public void endScope() throws AnalysisException {
		
		int numPops = 0;
		Iterator<Symbol> entries = symbolStack.iterator();
		
	    while (entries.hasNext()) {
	        
	        Symbol S = entries.next();
	        	        
	        if(S.getDecl() instanceof VarDecl && S.getScope() == Globals.class_scope && currScope == Globals.local_scope) { // If we're in local scope, don't delete class-level variables 
	        	// leave global variables on the symbol stack!!!
	        } else if(S.getScope() == currScope) {	        
	        	++numPops; // To avoid messing up our iterator, just keep track of the number of symbols to pop	        	
	        }
	    }
	    
	    for(int i = 0; i < numPops; ++i) {
	    	symbolStack.pop(); // OK, actually pop them now
	    }
		
	    // Decrement the scope unless doing so would cause the scope to drop below 0
		if((currScope - 1) < 0) {
			throw new AnalysisException("Decrementing scope level would cause scope to drop below 0");
		} else {
			--currScope;
		}
	}
	
	// Returns the current scope
	public int getScope() {
		return currScope;
	}
}
