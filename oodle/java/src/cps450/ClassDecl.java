/**
 * Main Class File:		Oodle.java
 * File:				ClassDecl.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

/**
 * ClassDecl is used mainly for checks using the instanceof operator
 * 
 * @author Alex Smith
 */
public class ClassDecl extends Declaration {

	/**
	 * ClassDecl constructor
	 * 
	 * @param t - The type of the class
	 */
	public ClassDecl(Type t) {
		super(t); 
	}

}
