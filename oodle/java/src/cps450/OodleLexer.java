/**
 * Main Class File:		Oodle.java
 * File:				OodleLexer.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

import java.io.*;

import cps450.oodle.lexer.Lexer;
import cps450.oodle.lexer.LexerException;
import cps450.oodle.node.*;

/**
 * OodleLexer class scans file(s) for tokens used in Oodle programs
 * 
 * @author Alex Smith
 */
// Lexes the specified Oodle file
public class OodleLexer extends Lexer {
	
	private String filename;
	
	// Constructor for the OoodleLexer class
	public OodleLexer(String filename) throws FileNotFoundException {
		super(new PushbackReader(new BufferedReader(new FileReader(filename)), 1024));
		this.filename = filename;
	}
	
	// Overridden filter method for printing out tokens and lexing errors
	protected void filter() throws LexerException, IOException {

		String message = "";
		
		// Token Error - Unrecognized character
		if (this.token instanceof TIllegalChar) {		
			message = filename + ":" + token.getLine() + "," + token.getPos() + ":" + "Unrecognized char:" + token.getText();
			throw new LexerException(message);
			
		// Token Error - Unterminated String
		} else if(this.token instanceof TUntermStrlit) {	
			message = filename + ":" + token.getLine() + "," + token.getPos() + ":" + "Unterminated string:" + token.getText();
			throw new LexerException(message);
			
		// Token Error - Illegal String
		} else if(this.token instanceof TIllegalStrlit) {	
			message = filename + ":" + token.getLine() + "," + token.getPos() + ":" + "Illegal string:" + token.getText();
			throw new LexerException(message);
			
		// Valid Token
		} else if (token != null && Options.getDSOption() && 
				(this.token instanceof TComment == false) && 
				(this.token instanceof TBlanks == false) && 
				(this.token instanceof EOF == false)) { // don't print/report comments or blanks
			
			// Format to print EOL
			if(this.token instanceof TEmptyLine) {
				message = "cr";				
				
			// Format to print identifier tokens	
			} else if(this.token instanceof TId) {
				message = "identifier:" + token.getText();				
				
			// Format to print keyword tokens	
			} else if(this.token instanceof TAnd || 
					this.token instanceof TBegin || 
					this.token instanceof TBoolean || 
					this.token instanceof TClassKw || 
					this.token instanceof TElse || 
					this.token instanceof TEnd || 
					this.token instanceof TFalse || 
					this.token instanceof TFrom || 
					this.token instanceof TIf || 
					this.token instanceof TInherits || 
					this.token instanceof TInt || 
					this.token instanceof TIs || 
					this.token instanceof TLoop || 
					this.token instanceof TMe || 
					this.token instanceof TNewKw || 
					this.token instanceof TNot || 
					this.token instanceof TNullKw || 
					this.token instanceof TOr || 
					this.token instanceof TString || 
					this.token instanceof TThen || 
					this.token instanceof TTrue || 
					this.token instanceof TWhile) {
				message = "keyword:" + token.getText();				
				
			// Format to print misc. tokens
			} else if(this.token instanceof TLPar  || 
					this.token instanceof TRPar  || 
					this.token instanceof TRBrkt  || 
					this.token instanceof TLBrkt || 
					this.token instanceof TComma || 
					this.token instanceof TSemicolon || 
					this.token instanceof TColon || 
					this.token instanceof TPeriod) {
				message = "\'" + token.getText() + "\'";				
				
			// Format to print operator tokens
			}else if(this.token instanceof TCatOp || 
					this.token instanceof TPlusOp || 
					this.token instanceof TMinusOp || 
					this.token instanceof TMultOp || 
					this.token instanceof TDivOp || 
					this.token instanceof TGtOp || 
					this.token instanceof TGteqOp || 
					this.token instanceof TEqOp || 
					this.token instanceof TAssignOp) {
				message = "operator:" + token.getText();				
			
			// Format to print string literal tokens
			} else if(this.token instanceof TStrlit) {
				message = "string lit:" + token.getText();				
			
			// Format to print all other tokens
			} else {
				message = token.getClass() + " " + token.getText();				
			}
			
			// Print the token to the user
			System.out.println(filename + ":" + token.getLine() + "," + token.getPos() + ":" + message);
		}
	}
}
