/**
 * Main Class File:		Oodle.java
 * File:				CodeGen.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import cps450.oodle.analysis.DepthFirstAdapter;
import cps450.oodle.node.*;
import cps450.Globals;
import cps450.MethodTracker;

/**
 * Class for producing Linux x86 assembly language from a decorated AST produced by the Oodle SemanticChecker
 * 
 * @author Alex Smith
 */

public class CodeGen extends DepthFirstAdapter {
	
	// CodeGen Class Variable Declarations
	// ************************************************	
	
	// PRINT STUFF - Constants etc. used to control and format the printing of assembly code
	PrintWriter pw;
	final String beginComment = "# ", endCode = "\n", space = " ", posSeparator = " ---> ";
	int indent = 0;
	
	// LABEL COUNTERS - Used to generate unique labels for if and loop statements
	int ifLabel = 0, whileLabel = 0;
	
	// FLAGS - used to control code generation
	boolean notIsPresent; // Used to invert the tests in for and loopwhile nodes
	boolean emitDataDirective; // Used to determine whether or not we should emit the .data directive before a variable declaration
	boolean isStart; // Used to check if a method is the main Oodle program method
	
	// MISC - see comments below
	private MethodTracker methTracker = Globals.methTracker; // Class used in generating code for Oodle methods
	
	// ************************************************
	
	/**
	 * CodeGen class constructor; creates the file instance for writing the assembled Oodle code to
	 */
	public CodeGen() {
		try {
			pw = new PrintWriter(new FileWriter(Globals.exeFilename));
		} catch (IOException e) {
			System.out.println("Unable to open " + Globals.exeFilename);
		}
	}
	
	/**
	 * Method for printing assembly code to a file
	 * 
	 * @param instr - String containing the assembly code to emit
	 */
	public void emitCode (String instr) {
		
		// Indent based on the current scope level
		int counter = 0;
		
		while(counter < indent) {
			instr = "\t" + instr;
			++counter;
		}
		
		// Print the instruction to the user's file
		pw.println(instr);		  
		pw.flush();
		  
		// Use for testing purposes
		// System.out.println(instr);
	}	
	
	/**
	 * Method for printing an assembly comment to a file
	 * 
	 * @param instr - String containing the assembly comment to emit
	 */
	public void emitComment (String instr) {
		
		// Indent based on the current scope level
		int counter = 0;
		
		while(counter < indent) {
			instr = "\t" + instr;
			++counter;
		}
		
		// Print the instruction to the user's file
		pw.println(instr);		  
		pw.flush();
		  
		// Use for testing purposes
		// System.out.println(instr);
	}
	
	 
	/**
	 * Overridden In methods, used to generate code for a node before visiting its children
	 * 
	 * @param node - The name of the node we're visiting
	 */
	@Override
	public void inAMethodDecl(AMethodDecl node) {
		
		// Create and emit a comment for this method before processing it
		String comment;
		String position = node.getStid().getLine() + ", " + node.getStid().getPos(),
			strtId = node.getStid().getText() + " ",
			lPar = node.getLPar().getText(),
			argDecls,
			rPar = node.getRPar().getText() + " ",
			opt_type,
			isKword = node.getIs().getText();
		
		if(node.getArgumentDeclList() != null) {
			argDecls = node.getArgumentDeclList().toString();
		} else {
			argDecls = "";
		}
		
		if(node.getOptType() != null) {
			opt_type = node.getOptType().toString() + " ";
		} else {
			opt_type = "";
		}
				
		comment = beginComment + position + posSeparator + strtId + lPar + argDecls + rPar + opt_type + isKword  + "(" + node.getClass() + ")";
		emitComment(comment);
		
		// Increase the indent of all code within the scope of this method
		++indent;		
	}	


	@Override
	public void inAClassDef(AClassDef node) {
		
		// Create and emit a comment for this class before processing it
		String comment;
		String position = node.getClassKw().getLine() + ", " + node.getClassKw().getPos();
		
		comment = beginComment + position + posSeparator + node.getClassKw() + node.getStid() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		emitCode(".data" + endCode);
	}
	
	
	/**
	 * Overridden Case methods, used to generate code for a node and control its traversal of its children
	 * 
	 * @param node - The name of the node we're visiting
	 */
	@Override
	public void caseAIfStmt(AIfStmt node) {
		
		String comment, code;
		String position = node.getI1().getLine() + ", " + node.getI1().getPos();
		++ifLabel; // Increment the global if statement counter
		int localIfLabel = ifLabel; // Store the global if statement counter in a local if statement counter
		
		comment = beginComment + position + posSeparator + node.getI1() + node.getExpr().toString() + " (" + node.getClass() + ")";
		emitComment(comment);
		
        inAIfStmt(node);
        if(node.getI1() != null)
        {
            node.getI1().apply(this);
        }
        if(node.getExpr() != null)
        {
            node.getExpr().apply(this);
        }
        
		code = "popl %eax";
		emitCode(code);
        
		// We need to invert the test if a NOT is present in the boolean test
		if(notIsPresent) {
			code = "cmpl $0, %eax";
			emitCode(code);
			notIsPresent = false;
		} else {
			code = "cmpl $1, %eax";
			emitCode(code);
		}
		
		code = "je trueLabel" + localIfLabel;
		emitCode(code);
		
		code = "jmp falseLabel" + localIfLabel;
		emitCode(code);
		
		code = "trueLabel" + localIfLabel + ":";
		emitCode(code);
		
		++indent; // increase the indent of the code within the if statement
        
        if(node.getThen() != null)
        {
            node.getThen().apply(this);
        }
        if(node.getN1() != null)
        {
            node.getN1().apply(this);
        }
        {
            List<PStmt> copy = new ArrayList<PStmt>(node.getStmt());
            for(PStmt e : copy)
            {
                e.apply(this);
            }
        }
        
		code = "jmp endIf" + localIfLabel;
		emitCode(code);
		
		--indent; // decrease the indent of this code
        
		code = "falseLabel" + localIfLabel + ":";
		emitCode(code);
		
		++indent; // increase the indent of this code
        
        if(node.getOptElse() != null)
        {
            node.getOptElse().apply(this);
        }
        
		code = "jmp endIf" + localIfLabel;
		emitCode(code);
		
		--indent; // decrease the indent of this code
        
        if(node.getEnd() != null)
        {
            node.getEnd().apply(this);
        }
        if(node.getI2() != null)
        {
            node.getI2().apply(this);
        }
        if(node.getN2() != null)
        {
            node.getN2().apply(this);
        }
        outAIfStmt(node);
        
		code = "endIf" + localIfLabel + ":";
		emitCode(code);
	}
	
	
	@Override
	public void caseALoopStmt(ALoopStmt node) {
		
		String comment, code;
		String position = node.getL1().getLine() + ", " + node.getL1().getPos();
		++whileLabel;
		int localWhileLabel = whileLabel;
		
		comment = beginComment + position + posSeparator + node.getL1() + node.getWhile() + node.getExpr().toString() + " (" + node.getClass() + ")";
		emitComment(comment);
		
        inALoopStmt(node);
        if(node.getL1() != null)
        {
            node.getL1().apply(this);
        }
        if(node.getWhile() != null)
        {
            node.getWhile().apply(this);
        }
        
		code = "loopWhile" + localWhileLabel + ":";
		emitCode(code);
		
		++indent;
        
		// Code to evaluate loop expressions
        if(node.getExpr() != null)
        {
            node.getExpr().apply(this);
        }
        
		comment = beginComment + position + posSeparator + node.getL1() + node.getWhile() + node.getExpr().toString() + " (" + node.getClass() + ")";
		emitComment(comment);
        
		code = "popl %eax";
		emitCode(code);
		
		// We need to invert the test if a NOT is present in the boolean test
		if(notIsPresent) {
			code = "cmpl $1, %eax";
			emitCode(code);
			notIsPresent = false;
		} else {
			code = "cmpl $0, %eax";
			emitCode(code);
		}
		
		code = "jne startLoopWhile" + localWhileLabel;
		emitCode(code);
		
		code = "jmp endLoopWhile" + localWhileLabel;
		emitCode(code);
		
		--indent;
		
		code = "startLoopWhile" + localWhileLabel + ":";
		emitCode(code);
		
		++indent;
        
        if(node.getN1() != null)
        {
            node.getN1().apply(this);
        }
        {
            List<PStmt> copy = new ArrayList<PStmt>(node.getStmt());
            for(PStmt e : copy)
            {
                e.apply(this);
            }
        }
        
		comment = beginComment + position + posSeparator + node.getL1() + node.getWhile() + node.getExpr().toString() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "jmp loopWhile" + localWhileLabel;
		emitCode(code);
		
		--indent;
		
		code = "endLoopWhile" + localWhileLabel + ":";
		emitCode(code);
        
        if(node.getEnd() != null)
        {
            node.getEnd().apply(this);
        }
        if(node.getL2() != null)
        {
            node.getL2().apply(this);
        }
        if(node.getN2() != null)
        {
            node.getN2().apply(this);
        }
        outALoopStmt(node);
	}
	
	
	@Override
	public void caseAMethodDecl(AMethodDecl node) {
		
		String code;		
		
		// We need to figure out whether or not we're processing the main Oodle method
		if(node.getStid().getText().trim().equals("start")) {
			isStart = true;
		} else {
			isStart = false;
		}
		
		// I've overridden this method above for simplicity's sake
        inAMethodDecl(node);
        
        if(node.getStid() != null)
        {
            node.getStid().apply(this);
        }
        if(node.getLPar() != null)
        {
            node.getLPar().apply(this);
        }
        if(node.getArgumentDeclList() != null)
        {
            node.getArgumentDeclList().apply(this);
        }
        if(node.getRPar() != null)
        {
            node.getRPar().apply(this);
        }
        if(node.getOptType() != null)
        {
            node.getOptType().apply(this);
        }
        if(node.getIs() != null)
        {
            node.getIs().apply(this);
        }
        if(node.getN1() != null)
        {
            node.getN1().apply(this);
        }
        {
            List<PVarDecl> copy = new ArrayList<PVarDecl>(node.getVarDecl());
            
            // If the list of vardecls contains some variable declarations, then we need to emit the .data directive            
            if (!copy.isEmpty()) {
            	emitDataDirective = true;
            }      
            
            for(PVarDecl e : copy)
            {
            	if(emitDataDirective) {
	    			code = ".data" + endCode;
	    			emitCode(code);
	    			emitDataDirective = false;
            	}
                e.apply(this);
            }
        }
        
        // The main Oodle method requires that we emit some special code
		if(isStart) {
			code = ".text\n.global main\nmain:" + endCode;
			emitCode(code);
		} else {
			int paramSpace = (methTracker.getNumParamsForMethod(node.getStid().getText()) * 4) + 4 + 4; // add an additional 4 bytes to make room for the dynamic link and then another 4 to store the return value
			code = ".text";
			emitCode(code);
			code = node.getStid().getText().trim() + ":";
			emitCode(code);
			code = "pushl %ebp"; // save old dynamic link
			emitCode(code);
			code = "movl %esp, %ebp";
			emitCode(code);
			code = "subl $" + paramSpace + ", %esp" + endCode; // create space for parameters
			emitCode(code);
		}
        
        if(node.getBegin() != null)
        {
            node.getBegin().apply(this);
        }
        if(node.getN2() != null)
        {
            node.getN2().apply(this);
        }
        {
            List<PStmt> copy = new ArrayList<PStmt>(node.getStmt());
            for(PStmt e : copy)
            {
                e.apply(this);
            }
        }
        if(node.getEnd() != null)
        {
            node.getEnd().apply(this);
        }
        if(node.getEndid() != null)
        {
            node.getEndid().apply(this);
        }
        if(node.getN3() != null)
        {
            node.getN3().apply(this);
        }
        outAMethodDecl(node);
        
        //clean up if this is any other method than the main method
        if(!isStart) {
			code = "movl -4(%ebp), %eax\t#put return value in EAX"; // pop local variables
			emitCode(code);
			code = "movl %ebp, %esp\t# clear off local variables"; // pop local variables
			emitCode(code);
			code = "pop %ebp\t# restore old BP";
			emitCode(code);
			code = "ret\t# return from method" + endCode;
			emitCode(code);
        }
        
        --indent;
	}
	

	@Override
	public void caseAArgumentDeclList(AArgumentDeclList node) {
		
        inAArgumentDeclList(node);
        {
            List<PArgDeclTail> copy = new ArrayList<PArgDeclTail>(node.getArgDeclTail());
            
            // Iterate through the list backward (makes dealing with parameters easier) if it is not empty
            if(!copy.isEmpty()) {
	            for (int i = copy.size() - 1; i >= 0; --i) {
	            	PArgDeclTail e = copy.get(i);
	                e.apply(this);
	            }
            }
        }
        if(node.getArgDecl() != null)
        {
            node.getArgDecl().apply(this);
        }
        outAArgumentDeclList(node);
	}
		
	@Override
	public void caseAExprList(AExprList node) {
        inAExprList(node);
        {
            List<PExprListTail> copy = new ArrayList<PExprListTail>(node.getExprListTail());
            
            // Iterate through the list backward (makes dealing with parameters easier) if it is not empty
            if(!copy.isEmpty()) {
	            for (int i = copy.size() - 1; i >= 0; --i) {
	            	PExprListTail e = copy.get(i);
	                e.apply(this);
	            }
            }
        }
        if(node.getExpr() != null)
        {
            node.getExpr().apply(this);
        }
        outAExprList(node);
	}

	/**
	 * Overridden Out methods, used to generate code for a node after we've visited it
	 * 
	 * @param node - The name of the node we're visiting
	 */
	@Override
	public void outAAddExpr4(AAddExpr4 node) {
		
		// Generate code for an addition expression
		String comment, code;
		String position = node.getPlusOp().getLine() + ", " + node.getPlusOp().getPos();	
		
		comment = beginComment + position + posSeparator + node.toString().trim() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "popl %ebx";
		emitCode(code);
		
		code = "popl %eax";
		emitCode(code);
		
		code = "addl %ebx, %eax";
		emitCode(code);
		
		code = "pushl %eax" + endCode;
		emitCode(code);
	}

	@Override
	public void outAAndExpr1(AAndExpr1 node) {
		
		// Generate code for an and expression
		String comment, code;
		String position = node.getAnd().getLine() + ", " + node.getAnd().getPos();
		
		comment = beginComment + position + posSeparator + node.toString() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "popl %ebx";
		emitCode(code);
		
		code = "popl %eax";
		emitCode(code);
		
		code = "andl %eax, %ebx";
		emitCode(code);
		
		code = "pushl %eax" + endCode;
		emitCode(code);
	}

	@Override
	public void outAAssignStmt(AAssignStmt node) {
		
		// Generate code for an assignment statement
		String comment, code;
		String position = node.getId().getLine() + ", " + node.getId().getPos();
		boolean isNameOfMethod = methTracker.isNameOfMethod(node.getId().getText());
		Symbol s = Globals.symbolMap.get(node.getId());
		
		comment = beginComment + position + posSeparator + node.toString().trim() + " (" + node.getClass() + ")";
		emitComment(comment);		
		
		if(isNameOfMethod) { // If we're doing an assignment to a method (basically, the return value from a call to a method gets stored in the calling method's name)
			code = "popl %eax";
			emitCode(code);
			code = "movl %eax, -4(%ebp)\t#put return value on to the stack" + endCode; // Put the return value from the call onto the stack
			emitCode(code);
		} else if(s != null) { // If we got a symbol out of the symbol map
			if(!s.isSymbolGlobalVar() && !s.isSymbolClassVar() && !s.isSymbolLocalVar()) { // If the variable we're assigning to isn't one of these types of variable, it's value needs to be stored on the stack
				VarDecl v = (VarDecl) s.getDecl();
				code = "popl %eax";
				emitCode(code);
				code = "movl %eax, " + v.getOffset() + "(%ebp)" + endCode;
				emitCode(code);
			} else { // The value we're assigning can simply be assigned to a variable name
				code = "popl %eax";
				emitCode(code);
				code = "movl %eax, var" + node.getId().getText() + endCode;
				emitCode(code);
			}
		}
	}

	@Override
	public void outACallExpr9(ACallExpr9 node) {
		
		// Generate code for an expression that is a call to a method
		String comment, code;
		String position = node.getId().getLine() + ", " + node.getId().getPos();
		int numParamsToPop = methTracker.getNumParamsForMethod(node.getId().getText()) * 4;
		
		comment = beginComment + position + posSeparator + node.toString() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "call " + node.getId().getText();
		emitCode(code);
		
		code = "addl $" + numParamsToPop + ", %esp"; // clean up the stack
		emitCode(code);
		
		code = "pushl %eax" + endCode; // push the result on to the stack
		emitCode(code);
	}

	@Override
	public void outACallStmt(ACallStmt node) {
		
		// Generate code for a statement that is a call to a method
		String comment, code;
		String position = node.getId().getLine() + ", " + node.getId().getPos();
		int numParamsToPop = methTracker.getNumParamsForMethod(node.getId().getText()) * 4;
		
		comment = beginComment + position + posSeparator + node.toString().trim() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "call " + node.getId().getText();
		emitCode(code);
		
		
		code = "addl $" + numParamsToPop + ", %esp" + endCode; // clean up the stack
		emitCode(code);
	}

	@Override
	public void outADivExpr5(ADivExpr5 node) {
		
		// Generate code for a division expression
		String comment, code;
		String position = node.getDivOp().getLine() + ", " + node.getDivOp().getPos();
		
		comment = beginComment + position + posSeparator + node.toString() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "popl %ebx"; // divisor
		emitCode(code);
		
		code = "movl $0, %edx"; // dividend
		emitCode(code);
		
		code = "popl %eax"; // dividend
		emitCode(code);
		
		code = "idivl %ebx, %eax"; // EAX = EDX:EAX / r/m dword
		emitCode(code);
		
		code = "addl %eax, %edx"; // dividend
		emitCode(code);
		
		code = "pushl %eax" + endCode; 
		emitCode(code);
	}

	@Override
	public void outAEqExpr2(AEqExpr2 node) {
		
		// Generate code for an expression to test if two expressions are equal
		String comment, code;
		String position = node.getEqOp().getLine() + ", " + node.getEqOp().getPos();
		
		comment = beginComment + position + posSeparator + node.toString() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "popl %ebx";				
		emitCode(code);
		
		code = "popl %eax";				
		emitCode(code);
		
		code = "cmpl %eax, %ebx";				
		emitCode(code);
		
		code = "seteb %al";				
		emitCode(code);
		
		code = "movzbl %al, %eax";				
		emitCode(code);
		
		code = "pushl %eax" + endCode;				
		emitCode(code);
	}

	@Override
	public void outAFalseLit(AFalseLit node) {
		
		// Generate code for the false literal (pushes 0 onto the stack)
		String comment, code;
		String position = node.getE().getLine() + ", " + node.getE().getPos();
		
		comment = beginComment + position + posSeparator + node.toString() + "(" + node.getClass() + ")";
		emitComment(comment);
		
		code = "pushl $0" + endCode;
		emitCode(code);
	}

	@Override
	public void outAGtEqExpr2(AGtEqExpr2 node) {
		
		// Generate code for a greater than or equal to comparison expression
		String comment, code;
		String position = node.getGteqOp().getLine() + ", " + node.getGteqOp().getPos();
		
		comment = beginComment + position + posSeparator + node.toString() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "popl %ebx";				
		emitCode(code);
		
		code = "popl %eax";				
		emitCode(code);
		
		code = "cmpl %ebx, %eax";				
		emitCode(code);
		
		code = "setgeb %al";				
		emitCode(code);
		
		code = "movzbl %al, %eax";				
		emitCode(code);
		
		code = "pushl %eax";				
		emitCode(code);
	}

	@Override
	public void outAGtExpr2(AGtExpr2 node) {
		
		// Generate code for a greater than comparison expression
		String comment, code;
		String position = node.getGtOp().getLine() + ", " + node.getGtOp().getPos();
		
		comment = beginComment + position + posSeparator + node.toString() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "popl %ebx";				
		emitCode(code);
		
		code = "popl %eax";				
		emitCode(code);
		
		code = "cmpl %ebx, %eax";				
		emitCode(code);
		
		code = "setgb %al";				
		emitCode(code);
		
		code = "movzbl %al, %eax";				
		emitCode(code);
		
		code = "pushl %eax";				
		emitCode(code);
	}

	@Override
	public void outAIdExpr9(AIdExpr9 node) {
		
		// Generate code for an expression that is an Oodle identifier
		String comment, code;
		String position = node.getE().getLine() + ", " + node.getE().getPos();		
		
		if(node.toString().trim().equals("out") || node.toString().trim().equals("in")) {
			return;
		} else {
			Symbol s = Globals.symbolMap.get(node);
			VarDecl v = (VarDecl) s.getDecl();
			
			comment = beginComment + position + posSeparator + node.toString() + " (" + node.getClass() + ")";
			emitComment(comment);
			
			if(!s.isSymbolGlobalVar() && !s.isSymbolClassVar() && !s.isSymbolLocalVar()) {
				code = "movl " + v.getOffset() + "(%ebp), %eax";
				emitCode(code);
				code = "pushl %eax" + endCode;
				emitCode(code);
			} else {
				code = "pushl var" + node.getE().getText() + endCode;				
				emitCode(code);
			}
		}
	}

	@Override
	public void outAIntLit(AIntLit node) {
		
		// Generate code for pushing an integer literal onto the stack
		String comment, code;
		String position = node.getE().getLine() + ", " + node.getE().getPos();
		
		comment = beginComment + position + posSeparator + node.toString() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "pushl $" + node.getE().getText() + endCode;
		emitCode(code);
	}

	@Override
	public void outAMultExpr5(AMultExpr5 node) {
		
		// Generate code for an Oodle multiplication expression
		String comment, code;
		String position = node.getMultOp().getLine() + ", " + node.getMultOp().getPos();
		
		comment = beginComment + position + posSeparator + node.toString() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "popl %eax";
		emitCode(code);
		
		code = "popl %ebx";
		emitCode(code);
		
		code = "imull %ebx";
		emitCode(code);
		
		code = "pushl %eax" + endCode; // EDX:EAX = EAX * r/m dword
		emitCode(code);
	}

	@Override
	public void outANegExpr6(ANegExpr6 node) {
		
		// Generate code for an expression that changes the sign of another expression or literal
		String comment, code;
		String position = node.getMinusOp().getLine() + ", " + node.getMinusOp().getPos();
		
		comment = beginComment + position + posSeparator + node.toString() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "popl %eax";
		emitCode(code);
		
		code = "negl %eax";
		emitCode(code);
		
		code = "pushl %eax" + endCode;
		emitCode(code);
	}

	@Override
	public void outANotExpr6(ANotExpr6 node) {
		notIsPresent = true; // true that we've encountered a not in an expression; changes the test in loops and if statements
	}

	@Override
	public void outAOrExpr(AOrExpr node) {
		
		// Generate code for an OR logical expression
		String comment, code;
		String position = node.getOr().getLine() + ", " + node.getOr().getPos();
		
		comment = beginComment + position + posSeparator + node.toString() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "popl %ebx";
		emitCode(code);
		
		code = "popl %eax";
		emitCode(code);
		
		code = "orl %eax, %ebx";
		emitCode(code);
		
		code = "pushl %eax" + endCode;
		emitCode(code);
	}

	@Override
	public void outAStart(AStart node) {
		
		// Generate code for finishing the execution of an Oodle program
		AClassDef cd = (AClassDef) node.getClassDef();
		String comment, code;
		String position = cd.getEndid().getLine() + ", " + cd.getEndid().getPos() ;
		
		comment = beginComment + position + posSeparator + "(" + node.getClass() + ")";
		emitComment(comment);
		
		code = "pushl $0";
		emitCode(code);
		
		code = "call exit" + endCode;
		emitCode(code);
	}

	@Override
	public void outASubExpr4(ASubExpr4 node) {
		
		// Generate code for a subraction expression
		String comment, code;
		String position = node.getMinusOp().getLine() + ", " + node.getMinusOp().getPos();	
		
		comment = beginComment + position + posSeparator + node.toString().trim() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = "popl %ebx";
		emitCode(code);
		
		code = "popl %eax";
		emitCode(code);
		
		code = "subl %ebx, %eax";
		emitCode(code);
		
		code = "pushl %eax" + endCode;
		emitCode(code);
	}

	@Override
	public void outATrueLit(ATrueLit node) {
		
		// Generate code for the true literal (pushes 1 onto the stack)
		String comment, code;
		String position = node.getE().getLine() + ", " + node.getE().getPos();
		
		comment = beginComment + position + posSeparator + node.toString() + "(" + node.getClass() + ")";
		emitComment(comment);
		
		code = "pushl $1" + endCode;
		emitCode(code);
	}

	@Override
	public void outAVarDecl(AVarDecl node) {
		
		// Generate code for a simple variable declaration
		String comment, code;
		String position = node.getId().getLine() + ", " + node.getId().getPos();
	
		comment = beginComment + position + posSeparator + node.toString().trim() + " (" + node.getClass() + ")";
		emitComment(comment);
		
		code = ".comm  var" + node.getId().getText() + ", 4, 4" + endCode;
		emitCode(code);
	}

	@Override
	public void outStart(Start node) {
		pw.close(); // close the printwriter as we've now reached the end of the .ood file(s)
	}
}
