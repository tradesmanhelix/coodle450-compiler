/**
 * Main Class File:		Oodle.java
 * File:				Type.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

/**
 * This class defines type constants and user-created types used in semantic checking
 * 
 * @author Alex Smith
 */
public class Type {
	
	// The types of types Oodle permits
    public static final Type 
    	ARRAY = new Type("array"),
    	BOOLEAN = new Type("boolean"),
    	ERROR = new Type("<error>"),
    	INT = new Type("int"),
    	STRING = new Type("string");
    
    protected String name; // Used for user-defined types (not implemented) 
    
    // Constructor
    protected Type(String name) {
        this.name = name;
    }
    
    // Returns the type of the object
    public String getType() {
    	return name;
    }
    
    // Converting a type to a string gives you a string with the type
	@Override
	public String toString() { 
		return name;
	}
}
