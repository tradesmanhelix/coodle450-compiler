/**
 * Main Class File:		Oodle.java
 * File:				MethodTracker.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

import java.util.HashMap;

import cps450.MethodDecl;

/**
 * Class for keeping track of all methods encountered during semantic checking.
 * Used during code generation for variable offsets, etc.
 * 
 * @author Alex Smith
 */
public class MethodTracker {
	
	// Data structure to keep track of methods encountered; the name of the method is used as the key
	private HashMap<String, MethodDecl> methList = new HashMap<String, MethodDecl>();
	
	/**
	 * Constructor
	 * 
	 */
	public MethodTracker() {
		
	}

	/**
	 * Add a method for the class to keep track of
	 * 
	 * @param newMeth
	 */
	public void addMethod(MethodDecl newMeth) {
		methList.put(newMeth.getName(), newMeth);
	}
	
	/**
	 * Get the number of parameters a method expects given the method's name
	 * 
	 * @param id - The name of the method we're concerned about
	 * @return - The number of parameters the method has
	 */
	public int getNumParamsForMethod(String id) {
		return methList.get(id).getNumParams();
	}
	 
	/**
	 * Looks up whether or not the given name is the name of a method 
	 * 
	 * @param id - The name to search for
	 * @return - Boolean result (true = is method name; false = is not a method name)
	 */
	public boolean isNameOfMethod (String id) {
		MethodDecl theDecl = methList.get(id);
		
		if(theDecl != null) {
			return true;
		} else {
			return false;
		}
	}
}
