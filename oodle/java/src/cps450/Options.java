/**
 * Main Class File:		Oodle.java
 * File:				OodleLexer.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

/**
 * Class for storing command line options passed to the Oodle compiler
 * 
 * @author Alex Smith
 */
public class Options {

	// Command line options belonging to this class
	private static boolean dsOption = false; // The -ds option specifies whether or not to print all tokens the lexer finds
	private static boolean SOption = false; // The -S option specifies whether or not to generate Linux assembly code for the Oodle source file
	private static boolean GOption = false; // The -S option specifies whether or not to generate source-level debugging for the given Oodle file

	
	// *** OPTIONS SETTER METHODS ***
 
	public static void setDSOption(boolean paramDsOption) {
		dsOption = paramDsOption;
	}
	
	public static void setSOption(boolean paramSOption) {
		SOption = paramSOption;
	}
	
	public static void setGOption(boolean paramGOption) {
		GOption = paramGOption;
	}
	
	
	// *** OPTIONS GETTER METHODS ***
	
	public static boolean getDSOption() {
		return dsOption;
	}
	
	public static boolean getSOption() {
		return SOption;
	}
	
	public static boolean getGOption() {
		return GOption;
	}
} // End of options class
