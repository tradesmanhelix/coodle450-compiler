/**
 * Main Class File:		Oodle.java
 * File:				VarDecl.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

/**
 * The variable declarations class is used in semantic checking and code generation.  VarDecls keep track of certain attributes
 * (i.e., scope) that are vital to code generation.
 * 
 * @author Alex Smith
 */
public class VarDecl extends Declaration {
	
	private int offset;	// The offset of a variable, computed during semantic checking

	// The constructor simply calls the Declaration constructor and initializes the offset for the variable
	public VarDecl(Type t, int offset) {
		super(t);
		this.offset = offset;
	}

	public int getOffset() {
		return offset;
	}
}
