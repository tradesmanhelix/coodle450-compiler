/**
 * Main Class File:		Oodle.java
 * File:				ArgDecl.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

/**
 * ArgDecls are a specialized type of variable declaration; the class is mainly used so that variable declarations can be
 * easily checked to see if they are more specialized than a simple variable declaration 
 * 
 * @author Alex Smith
 */
public class ArgDecl extends VarDecl {

	/**
	 * Constructor simply calls its super - VarDecl
	 * 
	 * @param t - The type of this argument declaration
	 * @param offset - The offset of this argument declaration (computed during semantic checking)
	 */
	public ArgDecl(Type t, int offset) {
		super(t, offset);
	}

}
