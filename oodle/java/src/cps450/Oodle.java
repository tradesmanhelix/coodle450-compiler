/**
 * Title:			Oodle.java
 * Author:			Alex Smith
 * Semester:		Spring 2012
 */
package cps450;

import cps450.oodle.node.*;
import cps450.Globals;
import cps450.oodle.parser.*;
import cps450.oodle.lexer.*;
import java.io.*;

/**
 * Main Oodle class which is responsible for handling the compilation of .ood source code files
 * 
 * @author Alex Smith
*/
public class Oodle {	
	
	/**
	 * Main oodle.jar program method which handles the flow of compiling .ood files (Oodle source files)
	 * 
	 * @param arguments - Command line arguments supplied to the Oodle compiler
	 * @throws ParserException - Thrown if an error occurs while parsing the file
	 * @throws IOException - Java I/O exception, generally related to reading/writing a file
	 * @throws LexerException - Thrown by the Oodle lexer, generally if it encounters an unrecognized token during the lexing phase of the compiler
	 */
	public static void main(String[] arguments) throws ParserException, IOException, LexerException {

		// Variable Initialization
		int error_count = 0; // Integer to keep track of the number of lexical and syntactic errors found
		Start node; // Node at which to begin the compilation process
		String response = "Compile failed"; // String used to report the results of compilation

		// Process the file(s) that the user specified on the command line, combining into one temporary file as necessary
		Globals.setFilename(processFiles(arguments));						

		// Create a new lexer and give it the name of the file to lex
		OodleLexer lexer = new OodleLexer(Globals.filename);
		
		// Invoke the SableCC Oodle parser on the lexed file	
		Parser parser = new Parser(lexer);
		
		// Attempt to lex and parse the file
		try {
			node = parser.parse();
			
			// If we haven't encountered any errors in the lexing and parsing phases, proceed to perform semantic checking
			SemanticChecker checker = new SemanticChecker();
			node.apply(checker);  // invoke SemanticChecker traversal
			
			// Only proceed to generate code for files which have not semantic errors
			if(Globals.getNumSemanticErrors() == 0) {
				CodeGen codeGenerator = new CodeGen();
				node.apply(codeGenerator);
				
				// Attempt to invoke the GCC compiler
				if(!Options.getSOption()) {
					try {
						String stdLib = System.getProperty("user.dir") + "/stdlib.o", exeName = "-o" + Globals.exeFilename.replace(".s", " ").trim();
						
						ProcessBuilder process = new ProcessBuilder("gcc", Globals.exeFilename, stdLib, exeName);	
						process.redirectErrorStream(true);
						
						Process proc = process.start();
						proc.waitFor();	
						
						InputStream shellIn = proc.getInputStream();
						int exitVal = proc.exitValue();
						
						if(exitVal == 0) {
							response = "GCC returned exite code 0 -- compile successful";
						} else {
							response = "GCC returned exite code " + exitVal + " with message:\n\n " + convertStreamToStr(shellIn);
						}
					} catch (IOException e) {
						e.printStackTrace();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}			
		} catch(ParserException P) { // Catch and report parser exceptions
			++error_count;
			System.out.println(Globals.filename + ":" + P.getToken().getLine() + "," + P.getToken().getPos() + ":" + "Error: At " + P.getMessage());
			
		} catch(LexerException L) { // Catch and report lexer exceptions
			++error_count;
			System.out.println(L.getMessage());
		} finally {
			
			// Change the response if we're only generating assembly and there were no errors (GCC not invoked)
			if(Options.getSOption() && error_count == 0 && Globals.getNumSemanticErrors() == 0) {
				response = "Code successfully generated";
			}
			
			// Display the error count from parsing and lexing to the user
			System.out.println("\n" + error_count + " lexical and syntactic error(s) found\n" + Globals.getNumSemanticErrors() + " semantic error(s) found");	
			System.out.println(response);
		}	
	}
		
	// Main Program Helper Methods
	// ************************************************
	
	/**
	 * Method for processing command line arguments and options and returning just the list of filenames to the main program
	 * 
	 * @param arguments - Copy of the command line arguments supplied to the main program
	 */ 
	public static String[] processArgs(String[] arguments) {

		String[] tempArgs;
		String[] localArguments;
		int numOptions = 0;
		
		localArguments = arguments;
		
		// Loop through the command line args and set any command line switches
		for(int i = 0; i < localArguments.length; ++i) {
			
			if (localArguments[i].equals("-ds")) {
				Options.setDSOption(true);
				++numOptions;
			} else if(localArguments[i].equals("-S")) {
				Options.setSOption(true);
				++numOptions;
			} else if(localArguments[i].equals("-g")) {
				Options.setGOption(true);
				++numOptions;
			}
		}
		
		tempArgs = new String[localArguments.length - numOptions];
		
		for (int j = numOptions; j < localArguments.length; ++j) {
			tempArgs[j - numOptions] = localArguments[j];
		}

		return tempArgs;
	}

	/**	
	 * Method for copying the contents of one file into another file
	 * 
	 * @param src - Source file; file to copy from
	 * @param dst - Destination file; file to copy to
	 * @param doAppend - Flag telling us whether or not to append to the contents of the destination file
	 * @throws IOException - Java I/O exception, generally related to reading/writing a file
	 */
	static void copyFile(String src, String dst, boolean doAppend) throws IOException {

		// Variable Initialization
		byte[] buf = new byte[1024];
		int len;
		File sourceFile = new File(src);
		File destFile = new File(dst);
		InputStream inStream = new FileInputStream(sourceFile);
		OutputStream outStream;

		// Copy to the destination file, appending if requested
		if (doAppend) {
			outStream = new FileOutputStream(destFile, true);
		} else {
			outStream = new FileOutputStream(destFile);
		}

		while ((len = inStream.read(buf)) > 0) {
			outStream.write(buf, 0, len);
		}

		inStream.close();
		outStream.close();
	}
	
	/**
	 * Method for processing the command line list of one or more files to be processed
	 * 
	 * @param arguments - Copy of the command line arguments supplied to the main program
	 * @return - Name of the file to write the compiled Oodle source code to
	 */
	static String processFiles(String[] arguments) {
		
		// Variable Initialization
		String filename;
		
		// Make sure that the user specified a filename
		if (arguments.length == 0) {
			System.out.println("usage:");
			System.out.println("java Oodle -ds -S -g filename(s)");
			System.exit(1);
		}					
		
		// If we're dealing with just one file and no options, simply set the name of the file
		if (arguments.length == 1) {

			filename = arguments[0];
			Globals.SetExeFilename(filename);
			
		} else { // Multiple Oodle source files or command line options are present
			
			// Initialization
			boolean append = false; // Initially, we don't want to append to the output file	
			filename = "temp.ood";
			
			// Process command line arguments (removes all options and leaves only filenames in arguments)
			arguments = processArgs(arguments);

			// Copy the contents of the specified files into a temporary file
			for (int i = 0; i < arguments.length; ++i) {

				try {
					copyFile(arguments[i], "temp.ood", append);

				} catch (IOException e) {
					System.err.println(e.getMessage());
					System.exit(1);
				}
				append = true; // Now we want to append to the Oodle source file
			}
			
			// Set the name of the executable
			Globals.SetExeFilename(arguments[arguments.length - 1]);
			Globals.setSourceFilename(arguments[arguments.length - 1]);
		}
		
		// Print the name(s) of the files we're working with
		System.out.println("Processing:");
		
		for (int j = 0; j < arguments.length; ++j) {
			System.out.println(arguments[j].toString() + " ");
		}
		
		System.out.println("\n----- Results -----\n");
		
		return filename;
	}

	/**
	* "To convert an InputStream to String, we use the Reader.read(char[]
	* buffer) method. We iterate until the Reader returns -1, which means
	* there's no more data to read. We use the StringWriter class to
	* produce the string."
	* 
	* @param is - Input stream to convert into a string
	*/
	public static String convertStreamToStr(InputStream is) throws IOException {
	 
		if (is != null) {
			Writer writer = new StringWriter();
	
			char[] buffer = new char[1024];
	
				try {
	
					Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
					int n;
	
					while ((n = reader.read(buffer)) != -1) {
						writer.write(buffer, 0, n);
					}
				} finally {
					is.close();
				}
			return writer.toString();
		} else {
			return "";
		}
	}
	
	// ************************************************
}// End of Oodle class
