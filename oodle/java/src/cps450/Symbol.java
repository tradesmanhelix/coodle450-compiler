/**
 * Main Class File:		Oodle.java
 * File:				Symbol.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

import java.util.ArrayList;
import cps450.oodle.node.Node;

/**
 * Symbols are compiler representations of tokens encountered in semantic processing.
 * They are used throughout the program, esp. in code generation as they store information
 * about variable offsets.
 * 
 * @author Alex Smith
 */
public class Symbol {	
	
	// Class variables - defines the symbol's
	private final String name;
	private final Declaration decl; // Class, method, variable, etc.
	private ArrayList attributes;
	private int scope;	
	private boolean isGlobalVar = false;
	private boolean isClassVar = false;
	private boolean isLocalVar = false;
	
	// Constructor
	public Symbol(String name, Declaration decl, int scope) {
		this.name = name;
		this.decl = decl;
		this.scope = scope;
		
		if(this.decl instanceof ArgDecl) {
			return;
		} 
		
		if(this.decl instanceof VarDecl && this.scope == Globals.global_scope) {
			isGlobalVar = true;
		} else {
			isGlobalVar = false;
		}
		
		if(this.decl instanceof VarDecl && this.scope == Globals.class_scope) {
			isClassVar = true;
		} else {
			isClassVar = false;
		}
		
		if(this.decl instanceof VarDecl && this.scope == Globals.local_scope) {
			isLocalVar = true;
		} else {
			isLocalVar = false;
		}
	}
	
	// Method used to set the attributes of a symbol
	public void setAttribute(Object O) {
		attributes.add(O);
	}
	
	// Symbol getter methods
	public int getScope() {
		return scope;
	}
		
	public String getName() {
		return name;
	}
	
	public Type getType() {
		return decl.getType();
	}
	
	public Declaration getDecl() {
		return decl;
	}
	
	public boolean isSymbolGlobalVar() {
		return isGlobalVar;
	}
	
	public boolean isSymbolClassVar() {
		return isClassVar;
	}
	
	public boolean isSymbolLocalVar() {
		return isLocalVar;
	}
}
