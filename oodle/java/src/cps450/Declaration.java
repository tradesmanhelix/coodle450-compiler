/**
 * Main Class File:		Oodle.java
 * File:				Declaration.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

/**
 * Declaration objects store information about their symbol. 
 * There is a hierarchy of Declaration classes.
 * 
 * @author Alex Smith
 */
public class Declaration {
	
	// Fundamentally, declarations have a type
	final protected Type type;
	
	public Declaration(Type t) {
		this.type = t;
	}
	
	public Type getType() {
		return type;
	}
}