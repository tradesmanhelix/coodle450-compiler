/**
 * Main Class File:		Oodle.java
 * File:				Globals.java
 * Semester:			Spring 2012
 * 
 * Author:				Alex Smith 
 */
package cps450;

import java.util.HashMap;

import cps450.oodle.node.Node;

/**
 * The global class contains static resources used throughout the rest of the Oodle compiler.
 * 
 * @author Alex Smith
 */
public class Globals {
	
	// Data structures
	public static MethodTracker methTracker = new MethodTracker(); // Global method tracker; populated during semantic checking, used during code generation
	public static SymbolTable symtab = new SymbolTable(); // Symbol table used during semantic checking
	public static HashMap<Node, Symbol>symbolMap = new HashMap<Node, Symbol>(); // map of symbols; populated during semantic checking, used during code generation
	
	// File processing
	public static String filename; // name of the file processed by the compiler; usually temp.ood
	public static String sourceFilename; // last .ood filename provided; used to name the executable and the .s file
	public static String exeFilename; // string used to name the executable file produced
	private static int numSemanticErrors; // Number of errors encountered during semantic processing
	
	// Integer constants (should be self-explanatory; used during semantic checking and code generation)
	public static final int global_scope = 0;
	public static final int class_scope = 1;
	public static final int local_scope = 2;
	public static final int no_offset = 0;
	
	// Global getter and setter methods; technically unnecessary (created early on before I fully understood the static keyword),
	// but left in to avoid breaking code as the project wraps up
	public static void setFilename(String filename) {
		Globals.filename = filename;
	}
	
	public static void setSourceFilename(String sourceFilename) {
		Globals.sourceFilename = sourceFilename;
	}
	
	public static void SetExeFilename(String exeFilename) {		
		Globals.exeFilename = exeFilename.replaceAll(".ood", ".s");
	}
	
	public static void incSemanticErrors() {
		++numSemanticErrors;
	}
	
	public static int getNumSemanticErrors() {
		return numSemanticErrors;
	}
}
