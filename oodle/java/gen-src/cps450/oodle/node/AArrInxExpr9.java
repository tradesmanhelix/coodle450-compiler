/* This file was generated by SableCC (http://www.sablecc.org/). */

package cps450.oodle.node;

import java.util.*;
import cps450.oodle.analysis.*;

@SuppressWarnings("nls")
public final class AArrInxExpr9 extends PExpr9
{
    private TId _quart_;
    private final LinkedList<POptArrInx> _optArrInx_ = new LinkedList<POptArrInx>();

    public AArrInxExpr9()
    {
        // Constructor
    }

    public AArrInxExpr9(
        @SuppressWarnings("hiding") TId _quart_,
        @SuppressWarnings("hiding") List<POptArrInx> _optArrInx_)
    {
        // Constructor
        setQuart(_quart_);

        setOptArrInx(_optArrInx_);

    }

    @Override
    public Object clone()
    {
        return new AArrInxExpr9(
            cloneNode(this._quart_),
            cloneList(this._optArrInx_));
    }

    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAArrInxExpr9(this);
    }

    public TId getQuart()
    {
        return this._quart_;
    }

    public void setQuart(TId node)
    {
        if(this._quart_ != null)
        {
            this._quart_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._quart_ = node;
    }

    public LinkedList<POptArrInx> getOptArrInx()
    {
        return this._optArrInx_;
    }

    public void setOptArrInx(List<POptArrInx> list)
    {
        this._optArrInx_.clear();
        this._optArrInx_.addAll(list);
        for(POptArrInx e : list)
        {
            if(e.parent() != null)
            {
                e.parent().removeChild(e);
            }

            e.parent(this);
        }
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._quart_)
            + toString(this._optArrInx_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._quart_ == child)
        {
            this._quart_ = null;
            return;
        }

        if(this._optArrInx_.remove(child))
        {
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._quart_ == oldChild)
        {
            setQuart((TId) newChild);
            return;
        }

        for(ListIterator<POptArrInx> i = this._optArrInx_.listIterator(); i.hasNext();)
        {
            if(i.next() == oldChild)
            {
                if(newChild != null)
                {
                    i.set((POptArrInx) newChild);
                    newChild.parent(this);
                    oldChild.parent(null);
                    return;
                }

                i.remove();
                oldChild.parent(null);
                return;
            }
        }

        throw new RuntimeException("Not a child.");
    }
}
