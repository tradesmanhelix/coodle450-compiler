package cps450.oodle.analysis;

@SuppressWarnings("serial")
public class AnalysisException extends Exception {
	
    private String message;

	public AnalysisException(String message)
    {		
        super(message);
        this.message = message;
    }

	@Override
	public String toString() {
		return this.message;
	}
}
