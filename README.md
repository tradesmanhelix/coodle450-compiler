coodle450-compiler
==================

Coodle450 is a compiler written in Java for the fictional programming language Oodle. I wrote Coodle450 for the class CpS 450 - Language Translation Systems at Bob Jones University in the Spring of 2012. More info is available in the Coodle450 User Manual, available here: https://docs.google.com/document/d/1BiX-FMmmwgfUYpQhvyWbgTl_s1LGqEmbbTMJhvCTIr4/edit.

Contact me via any of the methods listed at http://about.me/alexmsmith.

Peace!